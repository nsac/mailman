#!/bin/sh

set -e

[ "$DEBUG" = "true" ] && set -x

# Config checks
[ -z "$MAILMAN_URLHOST" ] && echo "Error: MAILMAN_URLHOST not set" && exit 128 || true
[ -z "$MAILMAN_EMAILHOST" ] && echo "Error: MAILMAN_EMAILHOST not set" && exit 128 || true
[ -z "$MAILMAN_ADMINMAIL" ] && echo "Error: MAILMAN_ADMINMAIL not set" && exit 128 || true
[ -z "$MAILMAN_ADMINPASS" ] && echo "Error: MAILMAN_ADMINPASS not set" && exit 128 || true

# Copy default mailman data from cache
if [ ! "$(ls -A /var/lib/mailman)" ]; then
   cp -a /var/lib/mailman.cache/. /var/lib/mailman/
fi

# Copy default spool from cache
if [ ! "$(ls -A /var/spool/postfix)" ]; then
   cp -a /var/spool/postfix.cache/. /var/spool/postfix/
fi

# If master list does not exist, create it
if [ ! -d /var/lib/mailman/lists/mailman ]; then
    /usr/lib/mailman/bin/newlist --quiet --urlhost=$MAILMAN_URLHOST --emailhost=$MAILMAN_EMAILHOST mailman $MAILMAN_ADMINMAIL $MAILMAN_ADMINPASS
fi

# Set mailman admin password
/usr/lib/mailman/bin/mmsitepass $MAILMAN_ADMINPASS

# Add virtualhost
linetoadd="add_virtualhost('$MAILMAN_URLHOST', '$MAILMAN_EMAILHOST')"
grep -q "$linetoadd" /usr/lib/mailman/Mailman/mm_cfg.py || echo "$linetoadd" >> /usr/lib/mailman/Mailman/mm_cfg.py

# Set default url and email hosts
if grep -q ^DEFAULT_URL_HOST /usr/lib/mailman/Mailman/mm_cfg.py; then
	sed -ie "s|^DEFAULT_URL_HOST.*$|DEFAULT_URL_HOST = '$MAILMAN_URLHOST'|" /usr/lib/mailman/Mailman/mm_cfg.py
else
	echo "DEFAULT_URL_HOST = '$MAILMAN_URLHOST'" >> /usr/lib/mailman/Mailman/mm_cfg.py
fi
if grep -q ^DEFAULT_EMAIL_HOST /usr/lib/mailman/Mailman/mm_cfg.py; then
	sed -ie "s|^DEFAULT_EMAIL_HOST.*$|DEFAULT_EMAIL_HOST = '$MAILMAN_EMAILHOST'|" /usr/lib/mailman/Mailman/mm_cfg.py
else
	echo "DEFAULT_EMAIL_HOST = '$MAILMAN_EMAILHOST'" >> /usr/lib/mailman/Mailman/mm_cfg.py
fi

# Set POSTFIX_STYLE_VIRTUAL_DOMAINS
if grep -q ^POSTFIX_STYLE_VIRTUAL_DOMAINS /usr/lib/mailman/Mailman/mm_cfg.py; then
	sed -ie "s|^POSTFIX_STYLE_VIRTUAL_DOMAINS.*|POSTFIX_STYLE_VIRTUAL_DOMAINS = ['$MAILMAN_EMAILHOST']|" /usr/lib/mailman/Mailman/mm_cfg.py
else
	echo "POSTFIX_STYLE_VIRTUAL_DOMAINS = ['$MAILMAN_EMAILHOST']" >> /usr/lib/mailman/Mailman/mm_cfg.py
fi

# 20MB size limit
postconf -e mailbox_size_limit=20971520

if [ -n "$MAILMAN_SSL_CRT" ] && [ -n "$MAILMAN_SSL_KEY" ] && [ -n "$MAILMAN_SSL_CA" ]; then
    postconf -e smtpd_use_tls=yes
    postconf -e smtpd_tls_cert_file=${MAILMAN_SSL_CRT}
    postconf -e smtpd_tls_key_file=${MAILMAN_SSL_KEY}
    postconf -e smtpd_tls_CAfile=${MAILMAN_SSL_CA}
fi

# Init Postfix Config
if [ -n "$VIRTUAL_ALIAS_DOMAINS" ]; then
	postconf -e virtual_alias_domains=$VIRTUAL_ALIAS_DOMAINS
fi
/usr/lib/mailman/bin/genaliases -q > /var/lib/mailman/data/aliases
chmod g+w /var/lib/mailman/data/aliases
chown root:mailman /var/lib/mailman

# Get personal aliases from database
if [ -n "$MYSQL_HOST" ] && [ -n "$MYSQL_DATABASE" ] && [ -n "$MYSQL_USER" ] && [ -n "$MYSQL_PASSWORD" ]; then
	if [ -n "$RELOCATED_SQL" ]; then
		cat > /etc/postfix/relocated <<EOF
hosts = $MYSQL_HOST
dbname = $MYSQL_DATABASE
user = $MYSQL_USER
password = $MYSQL_PASSWORD
query = $RELOCATED_SQL
EOF
		postconf -e relocated_maps=mysql:/etc/postfix/relocated
	fi

	if [ -n "$VIRTUAL_ALIAS_SQL" ]; then
		cat > /etc/postfix/virtual <<EOF
hosts = $MYSQL_HOST
dbname = $MYSQL_DATABASE
user = $MYSQL_USER
password = $MYSQL_PASSWORD
query = $VIRTUAL_ALIAS_SQL
EOF
		postconf -e virtual_alias_maps=hash:/var/lib/mailman/data/virtual-mailman,mysql:/etc/postfix/virtual
	fi
fi

# Fix permissions
chown root /var/spool/postfix
chown postfix:root \
  /var/spool/postfix/active \
  /var/spool/postfix/bounce \
  /var/spool/postfix/corrupt \
  /var/spool/postfix/defer \
  /var/spool/postfix/deferred \
  /var/spool/postfix/flush \
  /var/spool/postfix/hold \
  /var/spool/postfix/incoming \
  /var/spool/postfix/private \
  /var/spool/postfix/saved \
  /var/spool/postfix/trace
chown postfix:postdrop \
  /var/spool/postfix/maildrop \
  /var/spool/postfix/public
/usr/lib/mailman/bin/check_perms -f

# Cleanup stale pids incase we don't exit cleanly
rm -f /var/spool/postfix/pid/* /var/lib/mailman/locks/*
chown root /var/spool/postfix/pid

# start syslog
touch /var/log/mail.log
syslogd
tail -n 0 -f /var/log/mail.log &

# start mailman
/usr/lib/mailman/bin/mailmanctl -s -q start

# start fcgi
/usr/bin/spawn-fcgi -u nginx -g www-data -s /var/run/fcgiwrap.sock -n -- /usr/bin/fcgiwrap &

#start nginx
/usr/sbin/nginx

# start postfix
postfix start &

# start crond
/usr/sbin/crond -l 15

# start mailman-api
[ -n "$MAILMAN_API_ALLOWED_FROM" ] && /usr/local/bin/mailman-api --allow-from=$(getent hosts $MAILMAN_API_ALLOWED_FROM | cut -f 1 -d ' ') --bind=0.0.0.0:8000 &

trap "{postfix stop; /usr/lib/mailman/bin/mailmanctl stop}" SIGINT
trap "{postfix stop; /usr/lib/mailman/bin/mailmanctl stop}" SIGTERM
trap "{postfix stop; /usr/lib/mailman/bin/mailmanctl stop}" EXIT
trap "{postfix reload; /usr/lib/mailman/bin/mailmanctl reload}" SIGHUP

sleep 1

$@

wait
