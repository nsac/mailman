#!/usr/bin/env python

from datetime import date
from email.utils import parsedate
from mailbox import mbox
import os
import sys
sys.path.append("/usr/lib/mailman/")
from Mailman import Site

keepmonths = 3
currentday = date.today()
threshold = currentday.year*12 + currentday.month - keepmonths

keepmboxpath = "/var/lib/mailman/keep.mbox"

for listname in sorted(Site.get_listnames()):
    sys.stdout.write("{}: ".format(listname))
    sys.stdout.flush()
    mboxpath = Site.get_archpath(listname) + ".mbox/" + listname + ".mbox"
    if os.path.isfile(mboxpath):
        mailbox = mbox(mboxpath)
        keep = mbox(keepmboxpath)
        countall = 0
        countkept = 0
        for key, message in mailbox.iteritems():
            countall += 1
            parseddate = parsedate(message.get("Date"))
            year = parseddate[0]
            month = parseddate[1]
            if year*12 + month >= threshold:
                countkept += 1
                keep.add(message)
        keep.close()
        os.rename(keepmboxpath, mboxpath)
        if countkept < countall:
            os.system("/usr/lib/mailman/bin/arch --wipe {} &>/dev/null".format(listname))
        sys.stdout.write("kept {} of {} mails.\n".format(countkept, countall))
    else:
        sys.stdout.write("skipping since the mailbox does not exist.\n")
    sys.stdout.flush()
