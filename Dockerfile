# First download and compile Mailman

FROM python:2-alpine as mailmanbuilder

RUN addgroup mailman && \
    adduser -s /no/shell -D -H -G mailman mailman && \
    mkdir /usr/lib/mailman /var/lib/mailman && \
    chgrp mailman /usr/lib/mailman /var/lib/mailman && \
    chmod a+rx,g+ws /usr/lib/mailman /var/lib/mailman && \
    apk --no-cache add \
      gcc \
      libc-dev \
      make \
      nginx \
      wget && \
    pip install dnspython && \
    wget -O /tmp/mailman-2.1.39.tgz http://ftp.gnu.org/gnu/mailman/mailman-2.1.39.tgz && \
    tar -xzf /tmp/mailman-2.1.39.tgz -C /tmp && \
    cd /tmp/mailman-2.1.39 && \
    ./configure --prefix /usr/lib/mailman --with-var-prefix=/var/lib/mailman && \
    make install && \
    echo "DEFAULT_ADMIN_IMMED_NOTIFY = No" >> /usr/lib/mailman/Mailman/mm_cfg.py && \
    echo "DEFAULT_ARCHIVE_PRIVATE = 1" >> /usr/lib/mailman/Mailman/mm_cfg.py && \
    echo "DEFAULT_DMARC_MODERATION_ACTION = 1" >> /usr/lib/mailman/Mailman/mm_cfg.py && \
    echo "DEFAULT_DMARC_NONE_MODERATION_ACTION = Yes" >> /usr/lib/mailman/Mailman/mm_cfg.py && \
    echo "DEFAULT_GENERIC_NONMEMBER_ACTION = 0" >> /usr/lib/mailman/Mailman/mm_cfg.py && \
    echo "DEFAULT_LIST_ADVERTISED = No" >> /usr/lib/mailman/Mailman/mm_cfg.py && \
    echo "DEFAULT_MAX_MESSAGE_SIZE = 0" >> /usr/lib/mailman/Mailman/mm_cfg.py && \
    echo "DEFAULT_RESPOND_TO_POST_REQUESTS = No" >> /usr/lib/mailman/Mailman/mm_cfg.py && \
    echo "DEFAULT_SEND_REMINDERS = No" >> /usr/lib/mailman/Mailman/mm_cfg.py && \
    echo "DEFAULT_SERVER_LANGUAGE = 'nl'" >> /usr/lib/mailman/Mailman/mm_cfg.py && \
    echo "DEFAULT_SUBSCRIBE_POLICY = 3" >> /usr/lib/mailman/Mailman/mm_cfg.py && \
    echo "DEFAULT_URL_PATTERN = 'http://%s/'" >> /usr/lib/mailman/Mailman/mm_cfg.py && \
    echo "MTA = 'Postfix'" >> /usr/lib/mailman/Mailman/mm_cfg.py && \
    echo "OWNERS_CAN_DELETE_THEIR_OWN_LISTS = Yes" >> /usr/lib/mailman/Mailman/mm_cfg.py && \
    echo "REMOVE_DKIM_HEADERS = Yes" >> /usr/lib/mailman/Mailman/mm_cfg.py && \
    echo "VERP_DELIVERY_INTERVAL = 1" >> /usr/lib/mailman/Mailman/mm_cfg.py && \
    cd /usr/lib/mailman/messages && \
    rm -rf ar ast ca cs da de el es et eu fa fi fr gl he hr hu ia it ja ko lt no pl pt pt_BR ro ru sk sl sr sv tr uk vi zh_CN zh_TW

# Finally build the final image

FROM python:2-alpine AS release

LABEL maintainer="Richard Brinkman <server@nsac.alpenclub.nl>"

COPY --from=mailmanbuilder /usr/lib/mailman /usr/lib/mailman
COPY --from=mailmanbuilder /var/lib/mailman /var/lib/mailman
COPY inetutils-syslogd /etc/logrotate.d/inetutils-syslogd

RUN addgroup mailman && \
    addgroup www-data && \
    adduser -s /no/shell -D -H -G mailman mailman && \
    adduser -s /no/shell -D -H -G www-data www-data && \
    # install software
    apk --no-cache add \
      ca-certificates \
      fcgiwrap \
      glib \
      inetutils-syslogd \
      logrotate \
      libev \
      nginx \
      openrc \
      pflogsumm \
      postfix \
      postfix-mysql \
      spawn-fcgi && \
    pip install dnspython && \
    chmod 644 /etc/logrotate.d/inetutils-syslogd && \
    # configure nginx
    mkdir /run/nginx && \
    # configure mailman
    /usr/lib/mailman/bin/check_perms -f && \
    echo "0 0 1 * * /cleanmailboxes.py" >> /usr/lib/mailman/cron/crontab.in && \
    crontab -u mailman /usr/lib/mailman/cron/crontab.in && \
    # configure postfix
    postconf -e smtputf8_enable=no && \
    echo postmaster: root > /etc/aliases && \
    postalias /etc/aliases && \
    postconf -e alias_maps=hash:/etc/aliases,hash:/var/lib/mailman/data/aliases &&\
    postconf -e default_destination_concurrency_limit=5 &&\
    postconf -e default_destination_concurrency_negative_feedback=0.2 && \
    postconf -e default_destination_concurrency_positive_feedback=0.1 && \
    postconf -e destination_concurrency_feedback_debug=yes && \
    postconf -e myhostname=localhost && \
    postconf -e recipient_delimiter=+ && \
    postconf -e relayhost="[smtp.antispamcloud.com]:587" && \
    postconf -e smtpd_milters=inet:mail_dkim:8891 && \
    postconf -e smtp_tls_security_level=may && \
    postconf -e virtual_alias_maps=hash:/var/lib/mailman/data/virtual-mailman && \
    # Cache default dirs as template (must come after configuration)
    cp -a /var/lib/mailman /var/lib/mailman.cache && \
    cp -a /var/spool/postfix /var/spool/postfix.cache && \
    # install mailman-api
    pip install mailman-api && \
    sed -ie 's/mlist.ApprovedAddMember(userdesc, ack=True, admin_notif=True)/mlist.ApprovedAddMember(userdesc)/;s/mlist.ApprovedDeleteMember(address, admin_notif=False, userack=True)/mlist.ApprovedDeleteMember(address)/' /usr/local/lib/python2.7/site-packages/mailmanapi/api.py

COPY nginx.conf /etc/nginx/conf.d/default.conf
COPY *.sh cleanmailboxes.py /

EXPOSE 25 80 8000

ENTRYPOINT ["/entry.sh"]

CMD ["tail", "-n", "0", "-f", "/var/lib/mailman/logs/*"]

FROM release AS development
ARG RELAYHOST="[smtp.ziggo.nl]:25"
RUN postconf -e relayhost=$RELAYHOST

FROM release AS production
