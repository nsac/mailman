# Mailman

All in one docker container for GNU
[Mailman](http://www.gnu.org/software/mailman/index.html).

## Origin

This project is based upon the
[macropin/mailman](https://github.com/macropin/docker-mailman) Docker image.
It adds the following functionality:

* Sets the admin password and virtual hosts, allowing the creation of maillists
  using the web interface.
* Adds the mailman API.
* Adds the environment variable MAILMAN_API_ALLOWED_FROM (see below)
* Sets the Postfix relayhost to `683.smtp.antispamcloud.com:587`.

## Example Usage

```
docker run --rm -ti --name mailman \
  -p 80:80 -p 25:25 -p 8000:8000 \
  -e MAILMAN_URLHOST=www.example.com \
  -e MAILMAN_EMAILHOST=example.com \
  -e MAILMAN_ADMINMAIL=admin@example.com \
  -e MAILMAN_ADMINPASS=foo \
  docker.io/macropin/mailman
```

## Environment Configs

 - `MAILMAN_URLHOST` - Mailman url host eg `www.example.com`
 - `MAILMAN_EMAILHOST` - Mailman email host eg `example.com`
 - `MAILMAN_ADMINMAIL` - Mailman administrator email address eg `admin@example.com`
 - `MAILMAN_ADMINPASS` - Mailman administrator password
 - `MAILMAN_API_ALLOWED_FROM` = Hostname of the website that has access to the Mailman API

SSL options for opportunistic SMTP TLS:

 - `MAILMAN_SSL_CRT` - SSL Certificate (optional)
 - `MAILMAN_SSL_KEY` - SSL Key (optional)
 - `MAILMAN_SSL_CA` - SSL CA (optional)

## Status

Complete and working in production.
